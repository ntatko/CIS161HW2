#ifndef TATKON_STUDENTS_H_INCLUDED
#define TATKON_STUDENTS_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

/*============================================================================
	program:	tatkon_students.h		author:		Noah Tatko
	date:		2/25/16					email:		tatkon@fontbonne.edu

	other files in project:
		main.cpp
		tatkon_students.cpp
		students.txt

	purpose:
		To create a class structure for int main() in main.cpp that allows
		for the use of student objects to be entered into a school object.

==============================================================================*/

class Student {
	// Define variables for Student
        string name, id;
        float gpa;
        int credits;
        string years[4] = { "Freshman", "Sophomore", "Junior  ", "Senior  " };
        int year = 0;

    public:
	// Prototype functions for Student
        Student();
		~Student();
        void setId(string sId);
		void setName(string sName);
        void setCredits(int sCredits);
        void addCredits(int nCredits);
        void setGpa(float sGpa);
		void setValues(string sFn, string sLn, int sId, double sGpa, int sCred);
		bool honorStudent();
		bool probation();
        string getId();
        string getName();
		string getYear();
        string printOut();
        float getGpa();
        int getCredits();
};

class School {
	// Declare vector vairables for School
		vector < Student > entire;
		vector < Student > honor;
		vector < Student > probation;
		int high;
		int low;

	public:
	// Create prototypes for School
		School();
		~School();
		void addStudent(Student stud);
		void makeHonor();
		void makeProbation();
		void highLow();
		string getHonor();
		string getProbation();
};

#endif // TATKON_STUDENTS_H_INCLUDED
