Name:	Noah Tatko
Email:	tatkon@fontbonne.edu
Date:	2/25/16

(C) Copyright 2016 by Noah Tatko. All rights reserved.

Use these files with permission only. The following people and orginizations have
permission to posess and use these files:

-Noah Tatko
-Fontbonne University
-ABET Accrediation Board

These files are made to fulfill the requirements for the second homework project
for the class CIS 161, offered by Fontbonne University, Spring 2016. The teacher
is Dr. Samantha Warren. The files included are:

-README.txt (this document)
-tatkon_students.h
-tatkon_students.cpp
-main.cpp

These files are on a gitlab repo mainly because my computer was and is on the 
fritz, so I needed a place that I could backup my files. This readme and the copy-
right is mainly to prevent other students in that class from copying mine, despite
it being a public repo. If you are the abovementioned class, feel free to use my
repo as a source of inspiration, but do not copy anything from here.

Thanks,

Noah Tatko