
#include "tatkon_students.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>
#include <Windows.h>

using namespace std;

/*============================================================================
	program:	tatkon_students.cpp		author:		Noah Tatko
	date:		2/25/16					email:		tatkon@fontbonne.edu

	other files in project:
		main.cpp
		tatkon_students.h
		students.txt

	purpose:
		To create a class structure for int main() in main.cpp that allows
		for the use of student objects to be entered into a school object.

==============================================================================*/

// Add a student to school
void School::addStudent(Student stud) {
	entire.push_back(stud);
}

// Create the honor list
void School::makeHonor() {
	for (int i = 0; i < entire.size(); i++) {
		if (entire[i].getGpa() >= 3.5) {
			honor.push_back(entire[i]);
		}
	}
}

// Student constructor
Student::Student() {}

Student::~Student() {}

// Get functions for Student
string Student::getId() {
	return id;
}

string Student::getName() {
	return name;
}

int Student::getCredits() {
	return credits;
}

float Student::getGpa() {
	return gpa;
}

string Student::getYear() {
	return years[year];
}

// Set functions for Student
void Student::setName(string sName) {
	name = sName;
}

void Student::setId(string sId) {
	id = sId;
}

void Student::setCredits(int sCredits) {
	credits = sCredits;
	if (credits < 30) {
		year = 0;
	}
	else if (credits < 60) {
		year = 1;
	}
	else if (credits < 90) {
		year = 2;
	}
	else {
		year = 3;
	}
}

void Student::setGpa(float sGpa) {
	gpa = sGpa;
}

void Student::setValues(string sFn, string sLn, int sId, double sGpa, int sCred) {
	gpa = sGpa;
	name = sFn + " " + sLn;
	credits = sCred;
	id = sId;

	if (credits < 30) {
		year = 0;
	}
	else if (credits < 60) {
		year = 1;
	}
	else if (credits < 90) {
		year = 2;
	}
	else {
		year = 3;
	}
}

// Bool functions for Student
bool Student::honorStudent() {
	if (gpa > 3.5) {
		return true;
	}
	else {
		return false;
	}
}

bool Student::probation() {
	if (gpa < 2.5) {
		return true;
	}
	else {
		return false;
	}
}

// Display function for Student
string Student::printOut() {
	stringstream convert;
	convert << credits;
	string result = convert.str();

	stringstream conver;
	conver << gpa;
	string resul = conver.str();
	return "" + name + "\t" + id + "\t" + years[year] + "\t" + resul + "\t" + result;
}

// School constructor
School::School() {}

// School destructor
School::~School() {}

// Find the highest and lowest Gpa students
void School::highLow() {
	high = 0;
	for (int i = 1; i < honor.size(); i++) {
		if (entire[i].getGpa() < entire[high].getGpa()) {
			high = i;
		}
	}

	low = 0;
	for (int i = 1; i < probation.size(); i++) {
		if (entire[i].getGpa() > entire[low].getGpa()) {
			low = i;
		}
	}
}

// Return the honor list in string format
string School::getHonor() {
	string retr = "";
	retr += honor[high].printOut() + "\n";
	for (int i = 0; i < honor.size(); i++) {
		if (i != high) {
			retr += honor[i].printOut() + "\n";
		}
	}
	return retr;
}

// Make the probationary list
void School::makeProbation() {
	for (int i = 0; i < entire.size(); i++) {
		if (entire[i].getGpa() <= 2.5) {
			probation.push_back(entire[i]);
		}
	}
}

// Return the probationary list in string format
string School::getProbation() {
	string retr = "";
	retr += probation[low].printOut() + "\n";
	for (int i = 0; i < probation.size(); i++) {
		if (i != low) {
			retr += probation[i].printOut() + "\n";
		}
	}
	return retr;
}

