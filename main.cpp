
#include "tatkon_students.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>
#include <Windows.h>

using namespace std;

vector <string> StringSplit(string str, char chopper);

/*============================================================================
	program:	tatkon_students.h		author:		Noah Tatko
	date:		2/25/16					email:		tatkon@fontbonne.edu

	other files in project:
		tatkon_students.h
		tatkon_students.cpp
		students.txt

	purpose:
		To create a class structure for int main() in main.cpp that allows
		for the use of student objects to be entered into a school object.

==============================================================================*/

int main(void) {
	// Declare initial variables
	School fontbonne;
	char breaker = ',';

	// Read the file into an array
	vector < string > fileText;
	string text;
	ifstream myOther;
	string fileName = "student.txt";

	try {
		myOther.open(fileName.c_str());

		if (!myOther.is_open()) {
			throw - 1;
		}
	}
	catch (int errNum) {
		std::cout << "I'm sorry, " << fileName << " did not open. Please check the fileName on \n";
		std::cout << "line 35 of main.cpp.\n";
		system("pause");
		return errNum;
	}

	while (getline(myOther, text)) {
		fileText.push_back(text);
	}

	vector < Student > stude;
	stude.resize(fileText.size());

	for (int i = 0; i < fileText.size(); i++) {
		// get Name from file, add it to list of students
		int location = fileText[i].find(breaker);
		string in1 = fileText[i].substr(0, location);

		fileText[i].erase(0, location + 1);
		location = fileText[i].find(breaker);
		string in2 = fileText[i].substr(0, location);
		fileText[i].erase(0, location + 1);

		stude[i].setName(in1 + " " + in2);

		// get ID # from file, add it to list of students
		location = fileText[i].find(breaker);
		stude[i].setId(fileText[i].substr(0, location));
		fileText[i].erase(0, location + 1);

		// get gpa from file, add it to list of students
		location = fileText[i].find(breaker);
		string in3 = fileText[i].substr(0, location);
		stude[i].setGpa(atof(in3.c_str()));
		fileText[i].erase(0, location + 1);

		// get # of credit hours from file, add it to list of students
		stude[i].setCredits(atoi(fileText[i].c_str()));

		// add Student to School
		fontbonne.addStudent(stude[i]);
	}

	// Display the honor and probationary lists
	fontbonne.makeHonor();
	fontbonne.makeProbation();
	fontbonne.highLow();
	string honors = fontbonne.getHonor();
	string probie = fontbonne.getProbation();
	cout << "Honor Roll: \n";
	cout << "Name\t\tID #\tYear\t\tGPA\tCredit Hrs\n";
	cout << honors;

	cout << "\nProbationary \n";
	cout << probie;

	system("pause");
	return 1;
}

vector <string> StringSplit(string str, char chopper) {
	vector < string > yarn;
	int location = str.find(chopper);
	while (location > 0) {
		yarn.push_back(str.substr(0, location));
		str.erase(0, location + 1);
	}
	yarn.push_back(str);
	return yarn;
}